<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>
<div class="text-right" style="padding-right: 30px;padding-top: 20px">
	User : Sirinta Yui<br>
	<button type="button" class="btn btn-link" id="btn-change-pass">
		<small>Change Password</small>
	</button>
	<i class="fa fa-sign-out" aria-hidden="true" id="icon-logout"></i>
	
</div>

<div class="modal" tabindex="-1" role="dialog" id="modal-logout">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <p>คุณต้องการออกจากระบบหรือไม่</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="btn-logout-ok">
        	<i class="fa fa-check" aria-hidden="true"></i> OK
        </button>
        <button type="button" class="btn btn-basic" data-dismiss="modal" id="btn-logout-cancel">
        	<i class="fa fa-times" aria-hidden="true"></i> Cancel
        </button>
      </div>
    </div>
  </div>
</div>

<div class="modal" tabindex="-1" role="dialog" id="modal-change-pass">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <p>เปลี่ยนรหัสผ่าน</p>
      </div>
      <div class="modal-body">
		<form id="form-change-password">
			<div class="form-group">
			  <label for="pwd">รหัสผ่านเดิม :</label>
			  <input type="password" class="form-control" name="pwdOld" id="txt-pwd-old">
			</div>
			<div class="form-group">
			  <label for="pwd">รหัสผ่านใหม่:</label>
			  <input type="password" class="form-control" name="pwdNew" id="txt-pwd-new">
			</div>
			<div class="form-group">
			  <label for="pwd">รหัสผ่านใหม่อีกครั้ง:</label>
			  <input type="password" class="form-control" name="pwdNewAgain" id="txt-pwd-new-again">
			</div>
		</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="btn-change-pass-ok">
        	<i class="fa fa-check" aria-hidden="true"></i> OK
        </button>
        <button type="button" class="btn btn-basic" data-dismiss="modal" id="btn-change-pass-cancel">
        	<i class="fa fa-times" aria-hidden="true"></i> Cancel
        </button>
      </div>
    </div>
  </div>
</div>
<hr>
<jsp:include page="/jsp/include/include_js.jsp" flush="true"/>
<script type="text/javascript">

$(document).ready(function() {
	var logout 		= $('#icon-logout');
	var btnLogoutOk = $('#btn-logout-ok');
	var changePass 		= $('#btn-change-pass');
	var btnChangePassOk = $('#btn-change-pass-ok');
	var formChangePass 	= $('#form-change-password');
	
	logout.on('click',function(){
		$('#modal-logout').modal('show');
	});
	
	changePass.on('click',function(){
		$('#modal-change-pass').modal('show');
	});
	
	btnChangePassOk.on('click',function(){
		if(formChangePass.valid()){
			
		}
	});
	
	btnLogoutOk.on('click',function(){
		var pageRedirectTo = "${pageContext.request.contextPath}/jsp/login.jsp";
		redirectPage(pageRedirectTo,null);
	});
	
	formChangePass.validate({
		messages: { // mapping error massage
			pwdOld:{
				required : 'กรุณากรอกข้อมูล'
            },
            pwdNew:{
				required : 'กรุณากรอกข้อมูล'
            },
            pwdNewAgain:{
            	required : 'กรุณากรอกข้อมูล'
            }
        },
        rules: {
        	pwdOld: {
	            required: true
	        },
	        pwdNew:{
				required : true
            },
            pwdNewAgain:{
            	required : true
            }
	    },
	    // Hightlight error inputs
	    highlight: function (element) {
	        $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
	    },

	 	// Render success for each input type
	    success: function (element) {
	    	$(element).closest('.form-group').removeClass('has-error').addClass('has-success');
	    }
	});
});

</script>
</body>
</html>