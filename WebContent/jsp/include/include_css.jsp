<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%!String contextPath = null;%>
<%contextPath = request.getContextPath();%>
<link rel="stylesheet" href="${pageContext.request.contextPath}/plugin/css/bootstrap.min.css"  media="screen">
<link rel="stylesheet" href="${pageContext.request.contextPath}/plugin/css/customize.css"  media="screen">
<link rel="stylesheet" href="${pageContext.request.contextPath}/plugin/css/font-awesome.min.css"  media="screen">
<link rel="stylesheet" href="${pageContext.request.contextPath}/plugin/datatable/datatables.min.css"  media="screen">
<style>
.page-header {
    min-width: 100%;
    max-width: 100%;
    min-height:50px;
    max-height:100px;
    height:50px;
    width: 100%;
    background-color: #78C2AD !important;
    text-align: center;
    padding: 30px;
    top: 0;
}

.page-menu {
    min-width: 100px;
    max-width: 250px;
    width: 250px;
    float: left;
}

.page-body {
    padding-left: 10px;
    min-width: 500px;
    max-width: 1050px;
    width: 1050px;
    float: right;
}

.required {
	font-size: 22px;
    font-weight: bold;
    color: red;
}

label.error {
	color: #a94442;
	font-weight: normal;
}

</style>	