<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<script src="${pageContext.request.contextPath}/plugin/js/jquery.js"></script>
<script src="${pageContext.request.contextPath}/plugin/js/bootstrap.js"></script>
<script src="${pageContext.request.contextPath}/plugin/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/plugin/js/jquery.validate.js"></script>
<script src="${pageContext.request.contextPath}/plugin/datatable/datatables.min.js"></script>
<script type="text/javascript">

function redirectPage(location,args){
	var form = $('<form></form>');
    form.attr("method", "POST");
    form.attr("type", "json");
    form.attr("action", location);
    $.each( args, function( key, value ) {
        var field = $('<input></input>');
        field.attr("type", "hidden");
        field.attr("name", key);
        field.attr("value", value);
        form.append(field);
    });
    $(form).appendTo($(document.body)).submit();
};

$.extend( true, $.fn.dataTable.defaults, {
	"scrollX": false,
    "paging": true,
    "info": true,
    "pagingType": "full_numbers",
    "searching": true,
    "search": {
        "caseInsensitive": true
    },
    "ordering": true,
    "order": [0, 'asc'],
    "orderCellsTop": true,
    "lengthChange": true,
    "autoWidth": true,
    "responsive" : true,
    "dom": '<"top"f>rt<"bottom"lpi><"clear">',
    "columnDefs": [{
        "targets": 0,
        "className": "text-center",
        "render": function(data, type, row, meta) {
            return meta.row + meta.settings._iDisplayStart + 1;
        }
    }],
    "language": {
        "emptyTable": 'ไม่พบข้อมูล'
    }
} );

</script>