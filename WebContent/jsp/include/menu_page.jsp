<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>
<div class="page-menu">
	<ul class="nav nav-pills nav-stacked" id="nav-menu">
	  <li><a class="nav-link" href="${pageContext.request.contextPath}/jsp/pages/index.jsp">หน้าหลัก</a></li>
	  <li><a class="nav-link" href="${pageContext.request.contextPath}/jsp/pages/pageAddEditBoxInquiry.jsp">บันทึก/แก้ไขข้อมูลมาสเตอร์</a></li>
	  <li><a class="nav-link" href="#">ค้นหาข้อมูลออเดอร์</a></li>
	  <li><a class="nav-link" href="#">ออกรายงานยอดขาย</a></li>
	  <li><a class="nav-link" href="#">ออกรายงานรายการสอบถามสินค้า</a></li>
	  <li><a class="nav-link" href="#">ออกรายงานออเดอร์ที่สั่ง</a></li>
	</ul>
</div>
<script src="${pageContext.request.contextPath}/plugin/js/jquery.js"></script>
<script type="text/javascript">

	$('a.nav-link').on('click', function(){
		sessionStorage.setItem('mainmenu', $(this).attr('href'));
	});
	
	var mainmenu = sessionStorage.getItem('mainmenu');
	if(mainmenu){
		urlRegExp = new RegExp(mainmenu.replace(/\/$/, '') + "$");  
		$('a.nav-link').each(function () {
			if (urlRegExp.test(this.href.replace(/\/$/, ''))) {
				$(this).parent().addClass('active');
			}else{
				$(this).parent().removeClass('active');
			}
		});
	}
	
</script>
</body>
</html>