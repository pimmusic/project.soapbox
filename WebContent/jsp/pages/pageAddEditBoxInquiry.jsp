<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<jsp:include page="/jsp/include/include_css.jsp" flush="true"/>
<body>
<jsp:include page="/jsp/include/header_page.jsp" flush="true" />
<div class="container-fluid">
	<jsp:include page="/jsp/include/menu_page.jsp" flush="true" />
	<div class="page-body">
		<form name="form-box-inquiry" id="form-box-inquiry">
			<div class="row">
	  			<div class="col-md-6"></div>
	  			<div class="col-md-6 text-right">
					<button type="button" class="btn btn-primary text-right" id="btn-add-new-order">
						<i class="fa fa-plus fa-sm"></i>
						บันทึกข้อมูลออเดอร์ใหม่
					</button>
	  			</div>
	  		</div>
	  		<br>
	  		<h4 class="topic">รายการออเดอร์ที่รอการยืนยัน</h4>
	  		<table id="table-order-waitting-confirm" class="table color-secondary">
			    <thead>
		            <tr>
		                <th>No.</th>
		                <th>รหัสออเดอร์</th>
		                <th>ชื่อลูกค้า</th>
		                <th>สถานะ</th>
		                <th>แก้ไข</th>
		                <th>ยืนยัน</th>
		                <th>ยกเลิก</th>
		            </tr>
		        </thead>
		        <tbody></tbody>
			</table>
			
			<hr class="style">
			
	  		<h4 class="topic">รายการออเดอร์รอการจัดส่ง</h4>
	  		<table id="table-order-waitting-shipping" class="table color-primary">
			    <thead>
		            <tr>
		                <th>No.</th>
		                <th>รหัสออเดอร์</th>
		                <th>ชื่อลูกค้า</th>
		                <th>รอบการจัดส่ง</th>
		                <th>สถานะ</th>
		            </tr>
		        </thead>
		        <tbody></tbody>
			</table>
		</form>
	</div>
</div>

<jsp:include page="/jsp/include/include_js.jsp" flush="true" />
<script type="text/javascript">

$(document).ready(function() {
	var form = $('#form-box-inquiry');
	var btnAddNewOrder = form.find('#btn-add-new-order');
	var $table1 	= $('#table-order-waitting-confirm');
	var $table2 	= $('#table-order-waitting-shipping');
	var dataList	= [];
	var data		= {};
	data		= { "orderId": "TH01", "name": "กาญจนา ผลมูล","date" : "วันพฤหัสบดีที่ 24 ก.พ. 2561", "status": "waitting confirm"}; dataList.push(data);
	data		= { "orderId": "TH02", "name": "กาญจนา ผลมูล","date" : "วันพฤหัสบดีที่ 24 ก.พ. 2561", "status": "confirm"};dataList.push(data);
	data		= { "orderId": "TH03", "name": "กาญจนา ผลมูล","date" : "วันพฤหัสบดีที่ 24 ก.พ. 2561", "status": "create"};dataList.push(data);
	data		= { "orderId": "TH04", "name": "กาญจนา ผลมูล","date" : "วันพฤหัสบดีที่ 24 ก.พ. 2561", "status": "waitting"};dataList.push(data);
	data		= { "orderId": "TH05", "name": "กาญจนา ผลมูล","date" : "วันพฤหัสบดีที่ 24 ก.พ. 2561", "status": "confirm"};dataList.push(data);
	data		= { "orderId": "TH01", "name": "กาญจนา ผลมูล","date" : "วันพฤหัสบดีที่ 24 ก.พ. 2561", "status": "waitting confirm"}; dataList.push(data);
	data		= { "orderId": "TH02", "name": "กาญจนา ผลมูล","date" : "วันพฤหัสบดีที่ 24 ก.พ. 2561", "status": "confirm"};dataList.push(data);
	data		= { "orderId": "TH03", "name": "กาญจนา ผลมูล","date" : "วันพฤหัสบดีที่ 24 ก.พ. 2561", "status": "create"};dataList.push(data);
	data		= { "orderId": "TH04", "name": "กาญจนา ผลมูล","date" : "วันพฤหัสบดีที่ 24 ก.พ. 2561", "status": "waitting"};dataList.push(data);
	data		= { "orderId": "TH05", "name": "กาญจนา ผลมูล","date" : "วันพฤหัสบดีที่ 24 ก.พ. 2561", "status": "confirm"};dataList.push(data);
	data		= { "orderId": "TH01", "name": "กาญจนา ผลมูล","date" : "วันพฤหัสบดีที่ 24 ก.พ. 2561", "status": "waitting confirm"}; dataList.push(data);
	data		= { "orderId": "TH02", "name": "กาญจนา ผลมูล","date" : "วันพฤหัสบดีที่ 24 ก.พ. 2561", "status": "confirm"};dataList.push(data);
	data		= { "orderId": "TH03", "name": "กาญจนา ผลมูล","date" : "วันพฤหัสบดีที่ 24 ก.พ. 2561", "status": "create"};dataList.push(data);
	data		= { "orderId": "TH04", "name": "กาญจนา ผลมูล","date" : "วันพฤหัสบดีที่ 24 ก.พ. 2561", "status": "waitting"};dataList.push(data);
	data		= { "orderId": "TH05", "name": "กาญจนา ผลมูล","date" : "วันพฤหัสบดีที่ 24 ก.พ. 2561", "status": "confirm"};dataList.push(data);
	
	fnDisplayTable1(dataList);
	fnDisplayTable2(dataList);
    
	btnAddNewOrder.on('click',function(){
		var pageRedirectTo = "${pageContext.request.contextPath}/jsp/pages/pageAddEditBox.jsp";
		redirectPage(pageRedirectTo,null);
	});
	
	
	function fnDisplayTable1(){
		var table;
		table = $table1.DataTable({
	        data: dataList,
	        destroy: true,
	        responsive: true,
	        columns: [{
	        	data: 'id',
	        	className: 'text-center',
	            render: function (data, type, row, meta) {
	                return meta.row + meta.settings._iDisplayStart + 1;
	            }
	        }, {
	            data: 'orderId'
	        }, {
	            data: 'name'
	        }, {
	            data: 'status'
	        }, {
	            data: 'status',
	            className: 'text-center',
	            render: function(data, type, full, meta) {
	                return '<a href="javascript:void(0);" class="btn-edit"> <i class="fa fa-edit fa-sm"></i> </a>';
	            }
	        }, {
	            data: 'status',
	            className: 'text-center',
	            render: function(data, type, full, meta) {
	                return '<a href="javascript:void(0);" class="btn-edit"> <i class="fa fa-paper-plane-o fa-sm"></i> </a>';
	            }
	        }, {
	            data: 'status',
	            className: 'text-center',
	            render: function(data, type, full, meta) {
	                return '<a href="javascript:void(0);" class="btn-edit"> <i class="fa fa-times fa-sm"></i> </a>';
	            }
	        }],
	        columnDefs: [],
	        ordering: true
	    });
	}
	
	function fnDisplayTable2(){
		var table;
		table = $table2.DataTable({
	        data: dataList,
	        destroy: true,
	        responsive: true,
	        columns: [{
	        	data: 'id',
	        	className: 'text-center',
	            render: function (data, type, row, meta) {
	                return meta.row + meta.settings._iDisplayStart + 1;
	            }
	        }, {
	            data: 'orderId'
	        }, {
	            data: 'name'
	        }, {
	            data: 'date'
	        }, {
	            data: 'status'
	        }],
	        columnDefs: [],
	        ordering: true
	    });
	}
});

</script>
</body>
</html>