<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<jsp:include page="/jsp/include/include_css.jsp" flush="true"/>
<body>
<jsp:include page="/jsp/include/header_page.jsp" flush="true" />
<div class="container-fluid">
	<jsp:include page="/jsp/include/menu_page.jsp" flush="true" />
	<div class="page-body">
		<button type="button" class="btn">Basic</button>
		<button type="button" class="btn" disabled="disabled">Basic</button>
		
		<button type="button" class="btn btn-default">Default</button>
		<button type="button" class="btn btn-outline-default">Default</button>
		<button type="button" class="btn btn-default" disabled="disabled">Default</button>
		
		<button type="button" class="btn btn-primary">Primary</button>
		<button type="button" class="btn btn-primary" disabled="disabled">Primary</button>
		
		<button type="button" class="btn btn-secondary">Secondary</button>
		<button type="button" class="btn btn-secondary" disabled="disabled">Secondary</button>
		
		<button type="button" class="btn btn-success">Success</button>
		<button type="button" class="btn btn-success" disabled="disabled">Success</button>
		
		<button type="button" class="btn btn-info">Info</button>
		<button type="button" class="btn btn-info" disabled="disabled">Info</button>
		
		<button type="button" class="btn btn-outline-warning">Warning</button>
		<button type="button" class="btn btn-warning">Warning</button>
		<button type="button" class="btn btn-warning" disabled="disabled">Warning</button>
		
		<button type="button" class="btn btn-danger">Danger</button>
		<button type="button" class="btn btn-danger" disabled="disabled">Danger</button>
		
		<button type="button" class="btn btn-dark">Dark</button>
		<button type="button" class="btn btn-dark" disabled="disabled">Dark</button>
		
		<button type="button" class="btn btn-light">Light</button>
		<button type="button" class="btn btn-light" disabled="disabled">Light</button>
		
		<button type="button" class="btn btn-link">Link</button>
	</div>
</div>

<jsp:include page="/jsp/include/include_js.jsp" flush="true" />
</body>
</html>