<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<jsp:include page="/jsp/include/include_css.jsp" flush="true"/>
<body>
<jsp:include page="/jsp/include/header_page.jsp" flush="true" />
<div class="container-fluid">
	<jsp:include page="/jsp/include/menu_page.jsp" flush="true" />
	<div class="page-body">
		<form name="form-box-add-edit" id="form-box-add-edit">
			<div class="row">
	  			<div class="col-md-12">
	  			<h4>ข้อมูลกล่อง</h4>
	  			</div>
	  		</div>
	  		<div class="row header-line">
	  			<label>ข้อมูลพื้นฐาน</label>
	  		</div>
	  		<br>
	  		<div class="row">
	  			<div class="col-md-6 form-group">
	  				<label class="col-md-5 control-label text-right">ชื่อกล่อง <span class="required">*</span> : </label>
					<div class="col-md-7">
					    <input type="text" class="form-control" id="txt-box-name" name="boxName">
					</div>
	  			</div>
	  		</div>
	  		<div class="row">
	  			<div class="col-md-6 text-center">
	  				<img src="${pageContext.request.contextPath}/plugin/img/box.JPG" align="middle">
	  			</div>
	  			<div class="col-md-6">
	  				<div class="row">
	  					<div class="col-md-12 form-group">
			  				<label class="col-md-4 control-label text-right">ความกว้าง <span class="required">*</span> : </label>
							<div class="col-md-6">
							    <input type="text" class="form-control" id="txt-box-width" name="boxWidth">
							</div>
							<label class="col-md-2 control-label text-left">ซม.</label>
			  			</div>
			  			<div class="col-md-12 form-group">
			  				<label class="col-md-4 control-label text-right">ความยาว <span class="required">*</span> : </label>
							<div class="col-md-6">
							    <input type="text" class="form-control" id="txt-box-length" name="boxLength">
							</div>
							<label class="col-md-2 control-label text-left">ซม.</label>
			  			</div>
			  			<div class="col-md-12 form-group">
			  				<label class="col-md-4 control-label text-right">ความสูง : </label>
							<div class="col-md-6">
							    <input type="text" class="form-control" id="txt-box-hight" name="boxHight">
							</div>
							<label class="col-md-2 control-label text-left">ซม.</label>
			  			</div>
			  			<div class="col-md-12 form-group">
			  				<label class="col-md-4 control-label text-right">ความหนา : </label>
							<div class="col-md-6">
							    <input type="text" class="form-control" id="txt-box-thick" name="boxThick">
							</div>
							<label class="col-md-2 control-label text-left">ซม.</label>
			  			</div>
			  			<div class="col-md-12 form-group">
			  				<label class="col-md-4 control-label text-right">ปริมาตร : </label>
							<div class="col-md-6">
							    <input type="text" class="form-control" id="txt-box-size" name="boxSize">
							</div>
							<label class="col-md-2 control-label text-left">ลบ.ซม.</label>
			  			</div>
			  			<div class="col-md-12 form-group">
			  				<label class="col-md-4 control-label text-right">น้ำหนักกล่อง : </label>
							<div class="col-md-6">
							    <input type="text" class="form-control" id="txt-box-weight" name="boxWeight">
							</div>
							<label class="col-md-2 control-label text-left">กรัม</label>
			  			</div>
			  			<div class="col-md-12 form-group">
			  				<label class="col-md-4 control-label text-right">ราคา : </label>
							<div class="col-md-6">
							    <input type="text" class="form-control" id="txt-box-price" name="boxPrice">
							</div>
							<label class="col-md-2 control-label text-left">บาท</label>
			  			</div>
			  			<div class="col-md-12 text-right">
							<button type="button" class="btn btn-primary" id="btn-save">
								<i class="fa fa-check-square-o fa-sm"></i> บันทึก
							</button>
							<button type="button" class="btn btn-secondary" id="btn-cancel">
								<i class="fa fa-times fa-sm"></i> ยกเลิก
							</button>
						</div>
	  				</div>
	  			</div>
	  		</div>
	  		
	  		<br>
		</form>
	</div>
</div>

<jsp:include page="/jsp/include/include_js.jsp" flush="true" />
<script type="text/javascript">

$(document).ready(function() {
	var form = $('#form-box-add-edit');
	var btnSave = form.find('#btn-save');
	var btnCancel = form.find('#btn-cancel');
	
	
	btnSave.on('click',function(){
		if(form.valid()){
			
		}
	});
	
	btnCancel.on('click',function(){
		var pageRedirectTo = "${pageContext.request.contextPath}/jsp/pages/pageAddEditBoxInquiry.jsp";
		redirectPage(pageRedirectTo,null);
	});
	
	form.validate({
		messages: { // mapping error massage
			boxWidth:{
				required : 'กรุณากรอกข้อมูล'
            },
            boxLength:{
				required : 'กรุณากรอกความยาว'
            },
            boxName:{
            	required : 'กรุณากรอกชื่อกล่อง'
            }
        },
        rules: {
	    	boxWidth: {
	            required: true
	        },
	        boxLength:{
				required : true
            },
            boxName:{
            	required : true
            }
	    },
	    // Hightlight error inputs
	    highlight: function (element) {
	        $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
	    },

	 	// Render success for each input type
	    success: function (element) {
	    	$(element).closest('.form-group').removeClass('has-error').addClass('has-success');
	    }
	});
	
});

</script>
</body>
</html>