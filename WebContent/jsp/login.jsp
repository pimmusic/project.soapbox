<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<jsp:include page="/jsp/include/include_css.jsp" flush="true"/>
<title>Soap Box</title>
<style>
body {
  font-family: Arial, Helvetica, sans-serif;
}

/* style the container */
.container {
  position: relative;
  border-radius: 5px;
  background-color: #f2f2f2;
  padding: 20px 0 30px 0;
  width: 40%;
} 

/* style inputs and link buttons */
input,
.btn {
  width: 100%;
  padding: 12px;
  border: none;
  border-radius: 4px;
  margin: 5px 0;
  opacity: 0.85;
  display: inline-block;
  font-size: 20px;
  line-height: 20px;
  text-decoration: none; /* remove underline from anchors */
}

.col {
  width: 100%;
  margin: auto;
  padding: 0 50px;
  margin-top: 6px;
}

@media screen and (max-width: 650px) {
  .col {
    width: 100%;
    margin-top: 0;
  }
}
</style>
</head>
<body style="background-color: #F8F9F9 !important;padding-top: 7%">
<div class="container">
  <form>
    <div class="row" style="text-align:center">
      <h2 style="text-align:center">Login Soap Box</h2>
      <div class="col">
        <input type="text" name="username" placeholder="Username" required>
        <input type="password" name="password" placeholder="Password" required>
        <button type="button" class="btn btn-success" id="btn-logon">Login</button>
      </div>
    </div>
  </form>
</div>

</body>
<jsp:include page="/jsp/include/include_js.jsp" flush="true"/>
<script type="text/javascript">

$(document).ready(function() {
	var logon = $('#btn-logon');
	logon.on('click',function(){
		var pageRedirectTo = "${pageContext.request.contextPath}/jsp/pages/index.jsp";
		redirectPage(pageRedirectTo,null);
	});
	
});

</script>
</html>